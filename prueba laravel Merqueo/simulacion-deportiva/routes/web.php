<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CampeonatosController;
use App\Http\Controllers\EquiposController;
use App\Http\Controllers\JugadoresController;
use App\Http\Controllers\Simulacion;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* home */
Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

/* torneos */
Route::get('/torneos', [CampeonatosController::class, 'index']);
Route::get('/torneos/creacion', function () {
    return view('torneos/create/create');
});
Route::get('/torneos/crear', [CampeonatosController::class, 'create']);
Route::get('/torneos/edicion/{id}', [CampeonatosController::class, 'viewEdit']);
Route::get('/torneos/editar/{id}', [CampeonatosController::class, 'edit']);

/* equipos */
Route::get('/equipos', [EquiposController::class, 'index']);
Route::get('/equipos/creacion', function () {
    return view('equipos/create/create');
});
Route::get('/equipos/crear', [EquiposController::class, 'create']);
Route::get('/equipos/edicion/{id}', [EquiposController::class, 'viewEdit']);
Route::get('/equipos/editar/{id}', [EquiposController::class, 'edit']);

/* jugadores */
Route::get('/jugadores', [JugadoresController::class, 'index']);
Route::get('/jugadores/creacion', function () {
    return view('jugadores/create/create');
});
Route::get('/jugadores/crear', [JugadoresController::class, 'create']);
Route::get('/jugadores/edicion/{id}', [JugadoresController::class, 'viewEdit']);
Route::get('/jugadores/editar/{id}', [JugadoresController::class, 'edit']);

/* simulacion */
Route::get('/simulacion', [Simulacion::class, 'index']);
/* Route::get('/simulacion/grupos', [Simulacion::class, 'generateGroups']); */
