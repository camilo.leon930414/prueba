@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Jugadores</h1>
                <div class="d-flex flex-row-reverse">
                    <a class="btn btn-primary" href="/jugadores/creacion">Crear Nuevo Equipo</a>
                </div>
                <div class="pt-4">
                    <table class="table table-striped">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Nacionalidad</th>
                                <th>Dorsal</th>
                                <td>Edad</td>
                                <td>Posicion</td>
                                <td>Foto</td>
                                <td>Estado</td>
                                <td>Fecha de Creacion</td>
                                <td>Acción</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list as $equipo)
                                <tr>
                                    <td>{{ $equipo['id'] }}</td>
                                    <td>{{ $equipo['nombre'] }}</td>
                                    <td>{{ $equipo['apellido'] }}</td>
                                    <td>{{ $equipo['nacionalidad'] }}</td>
                                    <td>{{ $equipo['dorsal'] }}</td>
                                    <td>{{ $equipo['edad'] }}</td>
                                    <td>{{ $equipo['posicion'] }}</td>
                                    <td>
                                        <center>
                                            <img style="width:5%" src="{{ $equipo['foto'] }}" alt="foto">
                                        </center>
                                    </td>
                                    <td>{{ $equipo['estado'] == 3? 'retirado':'activo' }}</td>
                                    <td>{{ empty($equipo['created_at']) ? 'Sin fecha' : date('d-m-Y  H:i ', strtotime($equipo['created_at'])); }}</td>
                                    <td>
                                        <a class="btn btn-warning" href="/jugadores/edicion/{{$equipo['id']}}" >Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </main>
@endsection
