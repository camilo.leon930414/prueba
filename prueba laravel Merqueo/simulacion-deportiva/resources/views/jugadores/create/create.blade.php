@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Crear Nuevo Jugador</h1>
                <form action="/jugadores/crear">
                    <div class="row pt-4">
                        <div class="col-md-3">
                            <p>
                                <label for="nombre">Nombre del Jugador</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="apellido">Apellido del Jugador</label>
                                <input class="form-control" id="apellido" name="apellido" type="text" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="dorsal">Dorsal</label>
                                <input class="form-control" id="dorsal" name="dorsal" type="number" min="0" max="99" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="edad">Edad</label>
                                <input class="form-control" id="edad" name="edad" type="number" min="0" max="50" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="nacionalidad">Nacionalidad</label>
                                <input class="form-control" type="text" name="nacionalidad" id="nacionalidad" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="foto">Foto</label>
                                <input class="form-control" type="file" name="foto" id="foto">
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label for="posicion">Posicion</label>
                                <select class="form-control" name="posicion" id="posicion" required>
                                    <option value="DC" selected>DC</option>
                                    <option value="VOL">VOL</option>
                                    <option value="MD">MD</option>
                                    <option value="LI">LI</option>
                                    <option value="LD">LD</option>
                                    <option value="CC">CC</option>
                                    <option value="DF">DF</option>
                                    <option value="DD">DD</option>
                                    <option value="DI">DI</option>
                                    <option value="PO">PO</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button class="btn btn-success">Crear</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
