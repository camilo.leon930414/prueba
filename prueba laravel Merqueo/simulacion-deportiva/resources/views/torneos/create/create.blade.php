@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Crear Nuevo Torneo</h1>
                <form action="/torneos/crear">
                    <div class="row pt-4">
                        <div class="col-md-6">
                            <p>
                                <label for="nombre">Nombre del Torneo</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="num">Numero de equipos</label>
                                <select class="form-control" name="num" id="num" required>
                                    <option value="8" selected>8</option>
                                    <option value="16">16</option>
                                    <option value="32">32</option>
                                </select>
                            </p>
                        </div>
                        <!-- <div class="col-md-3">
                            <p>
                            <label for="estado">Estado</label>
                                <select class="form-control" name="estado" id="estado" required>
                                    <option value="1" selected>Activo</option>
                                </select>
                            </p>
                        </div> -->
                        <div class="col-md-6">
                            <p>
                                <label for="pais">País</label>
                                <input class="form-control" type="text" id="pais" name="pais" required>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label for="premio">Premio en Pesos Colombianos</label>
                                <input class="form-control" type="number" id="premio" name="premio" required>
                            </p>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button class="btn btn-success">Crear</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
