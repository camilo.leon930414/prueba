@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Torneos</h1>
                <div class="d-flex flex-row-reverse">
                    <a class="btn btn-primary" href="/torneos/creacion">Crear Nuevo Torneo</a>
                </div>
                <div class="pt-4">
                    <table class="table table-striped">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Número de Equipos</th>
                                <th>Premio</th>
                                <th>Pais</th>
                                <th>Estado</th>
                                <td>Fecha de Creacion</td>
                                <td>Acción</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list as $torneo)
                                <tr>
                                    <td>{{ $torneo['id'] }}</td>
                                    <td>{{ $torneo['nombre'] }}</td>
                                    <td>{{ $torneo['num_equipos'] }}</td>
                                    <td>${{ $torneo['premio'] }}</td>
                                    <td>{{ $torneo['pais'] }}</td>
                                    <td>{{ $torneo['estado'] == 1 ? 'Activo':'Acabado'  }}</td>
                                    <td>{{ empty($torneo['created_at']) ? 'Sin fecha' : date('d-m-Y  H:i ', strtotime($torneo['created_at'])); }}</td>
                                    <td>
                                        <a class="btn btn-warning" href="/torneos/edicion/{{$torneo['id']}}" >Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </main>
@endsection
