@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Editar Torneo</h1>
                <form action="/torneos/editar/{{$datos->id}}">
                    <div class="row pt-4">
                        <div class="col-md-4">
                            <p>
                                <label for="nombre">Nombre del Torneo</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" required value="{{$datos->nombre}}">
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p>
                                <label for="pais">País</label>
                                <input class="form-control" type="text" id="pais" name="pais" required value="{{$datos->pais}}">
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p>
                                <label for="premio">Premio en Pesos Colombianos</label>
                                <input class="form-control" type="number" id="premio" name="premio" required value="{{$datos->premio}}">
                            </p>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button class="btn btn-success">Actualizar</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
