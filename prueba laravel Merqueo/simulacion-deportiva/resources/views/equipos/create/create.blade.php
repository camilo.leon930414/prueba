@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Crear Nuevo Equipo</h1>
                <form action="/equipos/crear">
                    <div class="row pt-4">
                        <div class="col-md-3">
                            <p>
                                <label for="nombre">Nombre del Equipo</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="pais">País</label>
                                <input class="form-control" type="text" id="pais" name="pais" required>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="bandera">Bandera</label>
                                <input class="form-control" type="file" name="bandera" id="bandera">
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p>
                                <label for="favorito">Favorito (entra más cerca al 1 más favorito)</label>
                                <select class="form-control" name="favorito" id="favorito" required>
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button class="btn btn-success">Crear</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
