@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Equipos</h1>
                <div class="d-flex flex-row-reverse">
                    <a class="btn btn-primary" href="/equipos/creacion">Crear Nuevo Equipo</a>
                </div>
                <div class="pt-4">
                    <table class="table table-striped">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Pais</th>
                                <th>Bandera</th>
                                <td>Favorito</td>
                                <td>Fecha de Creacion</td>
                                <td>Acción</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list as $equipo)
                                <tr>
                                    <td>{{ $equipo['id'] }}</td>
                                    <td>{{ $equipo['nombre'] }}</td>
                                    <td>{{ $equipo['pais'] }}</td>
                                    <td>
                                        <center>
                                            <img style="width:5%" src="{{ $equipo['bandera'] }}" alt="foto">
                                        </center>
                                    </td>
                                    <td>{{ $equipo['num_favorito'] <=4  ? 'Favorito':'No Favorito ' }}</td>
                                    <td>{{ empty($equipo['created_at']) ? 'Sin fecha' : date('d-m-Y  H:i ', strtotime($equipo['created_at'])); }}</td>
                                    <td>
                                        <a class="btn btn-warning" href="/equipos/edicion/{{$equipo['id']}}" >Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </main>
@endsection
