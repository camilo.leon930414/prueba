@extends('layouts.app')

@section('content')
    <main>
        <section class="card">
            <div class="card-body">
                <h1>Simular Torneo</h1>
                <form action="/simulacion/grupos">
                    <div class="row pt-4">
                        <div class="col-md-3">
                            <select class="form-control" name="idTorneo" id="idTorneo" required>
                                @foreach ($list as $torneo)
                                    <option value="{{$torneo['id']}}" selected>{{ $torneo['nombre'].' -> Número de equipos: '.$torneo['num_equipos']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-9">
                            <button class="btn btn-primary">Crear Grupos</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection