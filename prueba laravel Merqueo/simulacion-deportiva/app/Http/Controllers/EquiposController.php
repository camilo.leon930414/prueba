<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Equipos;

class EquiposController extends Controller
{
    /* index */
    function index(Request $request){
        try{
            $db =  new Equipos();
            $list = $db::all()->toArray();
            return view("equipos/list/index")->with('list', $list);
        }catch (Exception $e) {
            \Log::info('Error lista'.$e);
            return \Response::json(['list error'], 500);
        }
    }
    /***** funcion para crear torneos ****/
    function create(Request $request){
        try{
            $new = new Equipos();
            $new->nombre = $request->nombre;
            $new->pais = $request->pais;
            $new->bandera = 'ejemplo';
            $new->num_favorito = $request->favorito;

            if($new->save()){
                return redirect('/equipos');
            }else{
                return \Response::json(['equipo no creado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create equip: '.$e);
            return \Response::json(['not list'], 500);
        }
    }

    function viewEdit($id){
        try{
            $equipo = new Equipos();
            $data = $equipo::find($id);
            return view('equipos/edit/edit')->with('datos',$data);
        }catch (Exception $e) {
            \Log::info('Error'.$e);
            return \Response::json(['not data'], 500);
        }
    }

    /***** funcion para editar equipos ****/
    function edit($id,Request $request){
        try{
            $equipo = new Equipos();
            $update = $equipo::find($id);
            $update->nombre = $request->nombre;
            $update->pais = $request->pais;
            /* $update->bandera = 'ejemplo'; */
            $update->num_favorito = $request->favorito;

            if($update->save()){
                return redirect('/equipos');
            }else{
                return \Response::json(['Campeonato no creado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create champion: '.$e);
            return \Response::json(['not list'], 500);
        }
    }
}
