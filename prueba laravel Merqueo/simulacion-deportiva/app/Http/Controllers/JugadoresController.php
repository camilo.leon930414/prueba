<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Jugadores;

class JugadoresController extends Controller
{
     /* index */
     function index(Request $request){
        try{
            $db =  new Jugadores();
            $list = $db::all()->toArray();
            return view("jugadores/list/index")->with('list', $list);
        }catch (Exception $e) {
            \Log::info('Error lista'.$e);
            return \Response::json(['list error'], 500);
        }
    }
    /***** funcion para crear jugadores ****/
    function create(Request $request){
        try{
            $new = new Jugadores();
            $new->nombre = $request->nombre;
            $new->apellido = $request->apellido;
            $new->dorsal = $request->dorsal;
            $new->edad = $request->edad;
            $new->nacionalidad = $request->nacionalidad;
            $new->posicion = $request->posicion;
            $new->foto = 'ejemplo';
            $new->estado = 1;

            if($new->save()){
                return redirect('/jugadores');
            }else{
                return \Response::json(['jugador no creado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create player: '.$e);
            return \Response::json(['not list'], 500);
        }
    }

    function viewEdit($id){
        try{
            $equipo = new Jugadores();
            $data = $equipo::find($id);
            return view('jugadores/edit/edit')->with('datos',$data);
        }catch (Exception $e) {
            \Log::info('Error'.$e);
            return \Response::json(['not data'], 500);
        }
    }

    /***** funcion para editar jugadores ****/
    function edit($id,Request $request){
        try{
            $jugador = new Jugadores();
            $update = $jugador::find($id);
            $update->nombre = $request->nombre;
            $update->apellido = $request->apellido;
            $update->dorsal = $request->dorsal;
            $update->edad = $request->edad;
            $update->nacionalidad = $request->nacionalidad;
            $update->posicion = $request->posicion;
            $update->foto = 'ejemplo';

            if($update->save()){
                return redirect('/jugadores');
            }else{
                return \Response::json(['jugador no editado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create champion: '.$e);
            return \Response::json(['not list'], 500);
        }
    }
}
