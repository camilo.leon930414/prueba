<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Campeonatos;

class CampeonatosController extends Controller
{
    /* index */
    function index(Request $request){
        try{
            $db =  new Campeonatos();
            $list = $db::all()->toArray();
            return view("torneos/list/index")->with('list', $list);
        }catch (Exception $e) {
            \Log::info('Error lista'.$e);
            return \Response::json(['list error'], 500);
        }
    }

    /***** funcion para crear torneos ****/
    function create(Request $request){
        try{
            $new = new Campeonatos();
            $new->nombre = $request->nombre;
            $new->num_equipos = $request->num;
            $new->pais = $request->pais;
            $new->estado = 1;
            $new->premio = $request->premio;

            if($new->save()){
                return redirect('/torneos');
            }else{
                return \Response::json(['Campeonato no creado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create champion: '.$e);
            return \Response::json(['not list'], 500);
        }
    }

    function viewEdit($id){
        try{
            $torneo = new Campeonatos();
            $data = $torneo::find($id);
            return view('torneos/edit/edit')->with('datos',$data);
        }catch (Exception $e) {
            \Log::info('Error'.$e);
            return \Response::json(['not data'], 500);
        }
    }

    /***** funcion para editar torneos ****/
    function edit($id,Request $request){
        try{
            $torneo = new Campeonatos();
            $update = $torneo::find($id);
            $update->nombre = $request->nombre;
            $update->pais = $request->pais;
            $update->premio = $request->premio;

            if($update->save()){
                return redirect('/torneos');
            }else{
                return \Response::json(['Campeonato no creado'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error create champion: '.$e);
            return \Response::json(['not list'], 500);
        }
    }
}
