<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartidoEquipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partido-equipo', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_partido')->unsigned()->index();
            $table->bigInteger('id_equipo')->unsigned()->index();
            $table->foreign('id_partido')->references('id')->on('partidos');
            $table->foreign('id_equipo')->references('id')->on('equipos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partido-equipo');
    }
}
