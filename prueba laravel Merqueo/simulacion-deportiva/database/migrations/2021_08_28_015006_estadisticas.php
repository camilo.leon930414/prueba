<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Estadisticas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadisticas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_resultado')->unsigned()->index();
            $table->bigInteger('id_jugador')->unsigned()->index();
            $table->foreign('id_jugador')->references('id')->on('jugadores');
            $table->foreign('id_resultado')->references('id')->on('resultado');
            $table->integer('goles');
            $table->integer('amarillas');
            $table->integer('rojas');
            $table->integer('asistencias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadisticas');
    }
}
