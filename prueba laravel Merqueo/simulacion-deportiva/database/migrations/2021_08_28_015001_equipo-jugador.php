<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EquipoJugador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipo-jugador', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_partido')->unsigned()->index();
            $table->bigInteger('id_jugador')->unsigned()->index();
            $table->foreign('id_partido')->references('id')->on('partidos');
            $table->foreign('id_jugador')->references('id')->on('jugadores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipo-jugador');
    }
}
