<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartidoResultado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partido-resultado', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_partido')->unsigned()->index();
            $table->bigInteger('id_resultado')->unsigned()->index();
            $table->foreign('id_partido')->references('id')->on('partidos');
            $table->foreign('id_resultado')->references('id')->on('resultado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partido-resultado');
    }
}
