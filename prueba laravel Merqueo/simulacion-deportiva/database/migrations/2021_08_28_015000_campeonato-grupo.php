<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CampeonatoGrupo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campeonato-grupo', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_campeonato')->unsigned()->index();
            $table->bigInteger('id_grupo')->unsigned()->index();
            $table->foreign('id_campeonato')->references('id')->on('campeonatos');
            $table->foreign('id_grupo')->references('id')->on('grupos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campeonato-grupo');
    }
}
